/* Jogo de vinte um versao 2.0
 * Licensa GNU/GPL versao 3.0
 * Copyright (c) 2013 by Prof. Ruben Carlo Benante <rcb@beco.cc>
 * Baseado em funcoes da biblioteca libeco.c
 * Utilizacao e modificacao autorizada, desde que se mantenha o nome do autor,
 * conforme descrito na licensa.
 * 
 * Data: 2013/10/19
 *
 * descricao da funcao:
 *                     int vinteumXX(tlist mao, tlist mesa);
 *       retorna:
 *              0 se nao quer mais carta
 *              1 se deseja mais uma carta
 *       recebe:
 *              lista de cartas na mao, e
*               lista de cartas na mesa
 *              (valor de A=1, 2, ..., 10, Q=10, J=10, K=10
 *
 * Para compilar:
 * $gcc vinteumv2.c -o vinteumv2 -ldl -lm -Wall -O0
 *
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <dlfcn.h>
#include <unistd.h>

#define MELHORDE 5.0

/* randnorep() magic (libeco.h) */
#define BASKETEMPTY  -1
#define BASKETFULL   -2
#define BASKETLISTED -3
#define BASKETERROR  -4
#define DRAWBASKET    0
#define LISTBASKET   -1

typedef struct slist
{
    int n;
    struct slist *prox;
} tlist;

int NFUNC;

int pontos(tlist *lmao); // calcula os pontos da mao
int baralhoinf(void); // sorteia uma carta com repeticao
int baralho52(int ini); //sorteia uma carta do baralho. ini=inicializa V/F
int sorte(int tipo, int ini); //1=baralho52, 0=baralhoinf
double changemm(double min1, double max1, double min2, double max2, double x1);
int randnorep(int n);
int randmm(int mini, int maxi);

void printlist(char *lname, tlist *pm);
void putlist(tlist **cmesa, int n);
void clearlist(tlist **cmesa);
void joinlist(tlist **cmesa, tlist **cmao);
void ordena(int no[], int to[]);

int main(int argc, char **argv)
{

    void *pluginlib;
    char fname[20]; /* nome das funcoes func1, func2... */
    char libname[80]; /* nome da biblioteca de funcoes */
    int jj0, jj1; /* todos contra todos ! */
    int i, s; /* indice, carta sorteada */
    int rodamao; /* melhor de 5 */

    int jog[2]; /* retorno das funcoes dos jogadores */
    int mao[2]; /* da mao */
    int rmao[2]; /* da rodada 5 maos */
    int tipo; //tipo de sorteio
    int sz; //tamanho do maior nome

    tlist *lmesa = NULL; /* cabeca das cartas na mesa */
    tlist *lmao0 = NULL; /* cabeca das cartas nas maos do jogador 0*/
    tlist *lmao1 = NULL; /* cabeca das cartas nas maos do jogador 1*/

    srand(time(NULL));

    if(argc>1) /* numero da libdilema a se usar */
    {
        if(!strcmp(argv[1], "-h"))
        {
            printf("usage:\n\t%s X Y\nwhere:\n\tX: number of libvinteumv2-X.so\n\tY: number of random type. 1- no repetition. 0-with repetition.\n", argv[0]);
            exit(0);
        }

        /* arg 1 */
        i=atoi(argv[1]);
        sprintf(libname, "./libvinteumv2-%d.so", i);
        pluginlib = dlopen(libname, RTLD_NOW);
        if(!pluginlib)
        {
            fprintf(stderr, "arg error:%s\n", dlerror());
            exit(EXIT_FAILURE);
        }
        else
            printf("arg %s ok.\n", libname);

        /* arg 2 */
        if(argc>2)
            tipo=atoi(argv[2]);
        if(tipo!=0 && tipo!=1)
            tipo=0; /* tipo 0 = baralho infinito, com repeticao. tipo 1 = baralho de 52 cartas, sem repeticao */
    }
    else /* sem argumentos */
    {
        for(i=5; i>=0; i--)
        {
            sprintf(libname, "./libvinteumv2-%d.so", i);
            pluginlib = dlopen(libname, RTLD_NOW);
            if(!pluginlib)
            {
                fprintf(stderr, "error:%s\n", dlerror());
                if(i==0)
                    exit(EXIT_FAILURE);
            }
            else
            {
                printf("%s ok.\n", libname);
                break;
            }
        }
        tipo=1; //default baralho de 52 cartas
    }
    dlerror();    /* Clear any existing error */
    printf("Tipo de baralho: %s\n\n", (tipo?"52 cartas":"infinito"));

    int (*pnfunc)(void), (*pnbig)(void);
    pnfunc=(int (*)(void)) dlsym(pluginlib, "nfunc"); //c99: *(void **) (&pnfunc) = dlsym(pluginlib, "nfunc");
    pnbig=(int (*)(void)) dlsym(pluginlib, "biggername");

    NFUNC=pnfunc();
    sz=pnbig();

    /* declaracao de vetor variavel: C99 */
    int (*jogador[NFUNC])(tlist *, tlist *); /* vetor de ponteiros para funcoes dos jogadores */
    int no[NFUNC]; /* indices de ptot ordenado */
    int tmao[NFUNC]; /* total */
    int tumpt[NFUNC]; /* se a pontuacao fosse 1 ponto por vitoria apenas */

    char *(*sn)(int)=(char *(*)(int)) dlsym(pluginlib, "playernames");

    //   inicializando
    for(i=0; i<NFUNC; i++)
    {
        tmao[i]=tumpt[i]=0; /* total pontos */
        no[i]=-1; /* indices ordenados */
    }

    /* le as funcoes para o ponteiro */
    for(i=0; i<NFUNC; i++)
    {
        sprintf(fname, "vinteum%d", i);
        jogador[i] = (int (*)(tlist *, tlist *)) dlsym(pluginlib, fname); /* padrao c89 */
        //    *(void **) (&jogador[i]) = dlsym(pluginlib, fname); /* padrao c99 */
    }

    for(jj0=0; jj0<NFUNC-1; jj0++)
        for(jj1=jj0+1; jj1<NFUNC; jj1++)
        {
            /* inicializa para novos adversarios */
            clearlist(&lmesa); // zera cartas na mesa
            s=sorte(tipo, 52); // Embaralha o deck
            for(i=0; i<2; i++)
                rmao[i]=0; /* acumulado de melhor de 5 */

            printf("Jog %2d %*s    x    Jog %2d %*s\n", jj0, sz, (*sn)(jj0), jj1, sz, (*sn)(jj1));
            for(rodamao=0; rodamao<MELHORDE; rodamao++) // melhor de 5
            {
                /* inicializa para nova mao */
                for(i=0; i<2; i++)
                    jog[i]=1; /* quero carta */
                do
                {
                    if(jog[0])
                    {
                        s=sorte(tipo,DRAWBASKET);
                        putlist(&lmao0, s); /* coloca carta na mao */
//          printlist("Mao0", lmao0);  /* descomente esta linha para debug */
                        jog[0]=jogador[jj0](lmao0, lmesa);// vinteum0(s, l);
                        mao[0]=pontos(lmao0); /* mao */
                    }
                    if(jog[1])
                    {
                        s=sorte(tipo,DRAWBASKET);
                        putlist(&lmao1, s); /* coloca carta na mao */
//          printlist("Mao1", lmao1);   /* descomente esta linha para debug */
                        jog[1]=jogador[jj1](lmao1, lmesa);
                        mao[1]=pontos(lmao1); /* mao */
                    }
                    /* importante: while considera pontuacao!
                     Mesmo que a funcao do jogador erre e
                     entre em loop pedindo mais cartas,
                     quando estourar 21 o laco para! */
                }
                while((jog[0] && mao[0]<=21) || (jog[1] && mao[1]<=21));

                // descarta as cartas da mao na mesa
                joinlist(&lmesa, &lmao0); /* passa as cartas da mao 0 para a mesa */
                joinlist(&lmesa, &lmao1); /* passa as cartas da mao 1 para a mesa */
//       printlist("Mesa", lmesa); /* descomente esta linha para debug */

                if(mao[0]!=mao[1]) // else empate nao da ponto
                {
                    if(mao[0]<=21 && mao[1]<=21) // sao validos!
                    {
                        if(mao[0]>mao[1]) //mao 0 ganhou
                        {
                            tmao[jj0] += mao[0]; /* total geral */
                            rmao[0] += mao[0]; /* total da rodada */
                            tumpt[jj0]++;
                        }
                        else //mao 1 ganhou
                        {
                            tmao[jj1] += mao[1];
                            rmao[1] += mao[1];
                            tumpt[jj1]++;
                        }
                    }
                    else // alguem estourou
                    {
                        if(mao[0]<=21) // jogador 0 nao estourou
                        {
                            tmao[jj0] += mao[0];
                            rmao[0] += mao[0];
                            tumpt[jj0]++;
                        } //apesar de nao precisar do else, sao mutuamente exclusivos
                        if(mao[1]<=21) // jogador 1 nao estourou
                        {
                            tmao[jj1] += mao[1];
                            rmao[1] += mao[1];
                            tumpt[jj1]++;
                        }
                    }
                }

                printf("Mao %d, Jogador %2d (total %3d)==> %2d x %2d <==Jogador %2d (total %3d)\n", rodamao+1, jj0, tmao[jj0], mao[0], mao[1], jj1, tmao[jj1]);
            } /* roda mao */
            printf("Jogador %2d %*s: total pontos: %3d, total vitorias: %3d, nesta rodada: %2d\n", jj0, sz, (*sn)(jj0), tmao[jj0], tumpt[jj0], rmao[0]);
            printf("Jogador %2d %*s: total pontos: %3d, total vitorias: %3d, nesta rodada: %2d\n", jj1, sz, (*sn)(jj1), tmao[jj1], tumpt[jj1], rmao[1]);
            printlist("Mesa", lmesa); /* mesa ao final das 5 maos */
            printf("\n");
        } /* jj1 / jj0 : fim do todos contra todos */

    /* ordena apenas os indices em no[] */
    ordena(no, tmao);

    /* imprime vencedores */
    printf("Classificacao final: (baralho: %s)\n", (tipo?"52 cartas":"infinito"));
    for(i=0; i<NFUNC; i++)
        printf("Jogador %02d %*s: total de pontos = %4d, media por jogo %6.2f, total vitorias: %2d\n", no[i], sz, (*sn)(no[i]), tmao[no[i]], tmao[no[i]]/(float)((NFUNC-1)*MELHORDE), tumpt[no[i]]);

    return 0;
}

/* retorna os pontos feitos na mao
As tem o maior valor que da uma soma menor ou igual a 21 */
int pontos(tlist *lmao)
{
    int as=0, cartas=0;

    while(lmao!=NULL)
    {
        cartas += lmao->n; // acumulo cartas
        if(lmao->n==1) // tenho as
            as = 1;
        lmao = lmao->prox;
    }

    if((cartas + 10 <= 21) && as)
        return cartas + 10;
    return cartas;
}

/* retorna os pontos feitos na mao (versao antiga)
As vale  11 se tiver carta 10, J, Q, K, ou vale 1 caso contrario */
/*
int pontos(tlist *lmao)
{
  int as=0, dez=0, outras=0;

  while(lmao!=NULL)
  {
      if(lmao->n==1) // conto as
          as++;
      else
          if(lmao->n==10) // conto 10
             dez++;
        else
             outras+=lmao->n; // acumulo outras
      lmao=lmao->prox;
  }
  return outras + dez*10 + as*(dez?11:1);
}
*/

void ordena(int no[NFUNC], int to[NFUNC])
{

    int i, j, k, ja;
    int max, imax;

    for(i=0; i<NFUNC; i++)
    {
        max=-1;
        imax=-1;
        for(j=0; j<NFUNC; j++)
            if(to[j]>max)
            {
                ja=0;
                for(k=0; k<i; k++) /* olhar todos anteriores */
                    if(no[k]==j)  /* ja esta usado ? */
                    {
                        ja=1;
                        break; /* nao obrigatorio. para otimizar */
                    }
                if(!ja)
                {
                    imax=j;
                    max=to[j];
                }
            }
        no[i]=imax; /* achou o maior ainda nao usado */
    }
}

int sorte(int tipo, int ini)
{
    if(tipo) //baralho 52
        return baralho52(ini);
    else //baralho infinito
        return baralhoinf();
}

/* sorteia uma carta com repeticao (baralho infinito) */
int baralhoinf(void)
{
    int s;

    s=rand()%13+1;
    return (s>10?10:s);
}

/* sorteia uma carta do baralho de 52 cartas. ini==52 p/ reembaralhar. */
int baralho52(int ini)
{
    int carta;

    if(ini==DRAWBASKET)
    {
        carta=randnorep(DRAWBASKET)%13+1;
        return (carta>10?10:carta);
    }
    else
    {
        randnorep(ini); /* cartas numeradas de 0 a 51 */
        return -1;
    }
}

/* imprime as cartas de uma dada lista */
void printlist(char *ln, tlist *pm)
{

    printf("%s", ln);
    while(pm!=NULL)
    {
        printf("->%d", pm->n);
        pm=pm->prox;
    }
    printf("->null\n");
}

/* insere carta no final de uma lista */
void putlist(tlist **cl, int n)
{
    /* acha o ultimo */
    tlist *este=*cl;
    tlist *ant=NULL;

    while(este != NULL)
    {
        ant=este;
        este=este->prox;
    }

    este = (tlist *) malloc(sizeof(tlist));
    este->prox = NULL;
    este->n = n;

    if(ant==NULL) // primeiro elemento, muda cabeca
        *cl = este;
    else
        ant->prox = este;
    return;
}

/* descarta da mao para a mesa */
void joinlist(tlist **cmesa, tlist **cmao)
{
    tlist *este=*cmao;

    while(este!=NULL)
    {
        putlist(cmesa, este->n); /* coloca este na mesa */
        este=este->prox; /* avanca na mao */
    }
    clearlist(cmao);
}

/* zera a lista dada */
void clearlist(tlist **cmesa)
{
    tlist *este=*cmesa;
    tlist *prox=NULL;

    while(este!=NULL)
    {
        prox=este->prox;
        free(este);
        este=prox;
    }
    *cmesa=NULL;
    return;
}

/* retorna inteiro entre [min,max[ (libeco.h) */
int randmm(int mini, int maxi)
{
    static int n1 = 0;
    int sorteio;

    if(!n1)
    {
        time_t seed = time(NULL);

        srand(seed);
        n1 = 1;
    }
    sorteio = (int) changemm(0.0, (double) RAND_MAX, (double) mini, (double) maxi, (double) rand());
    if(sorteio == maxi)
        sorteio--;
    return sorteio;
}

/* retorna inteiro aleatorio sem repeticao entre [0, n[ (libeco.h) */
int randnorep(int n)
{
    struct listnum
    {
        int num;                /* the number */
        struct listnum *prox;   /* next number */
    };
    static int listqtd = 0;     /* how much numbers in the basket */
    static struct listnum *lcab = NULL; /* head of the list */
    struct listnum *ln = NULL, *lant = NULL;    /* ln: list point to number. lant: list point to the node before. */
    static struct listnum *lista = NULL; /* indice para listar todos elementos */

    int j;
    int sort;

    if(lcab == NULL)
    {
        lcab = (struct listnum *) malloc(sizeof(struct listnum));
        lcab->prox = NULL;
        lista = lcab;
    }

    if(n > 0)                   /* fills the basket */
    {
        /* deleting the old basket */
        lant = lcab->prox;
        while(lant != NULL)     /* delete previous experiment */
        {
            ln = lant->prox;
            free(lant);
            lant = ln;
        }
        lcab->prox = NULL;

        /* filling the new basket */
        listqtd = n;
        ln = lcab;
        for(j = 0; j < listqtd; j++)
        {
            ln->num = j;
            ln->prox = (struct listnum *) malloc(sizeof(struct listnum));
            lant = ln;
            ln = ln->prox;
            ln->prox = NULL;
        }
        free(ln);
        lant->prox = NULL;

        return BASKETFULL;
    }
    if(n == DRAWBASKET)         /* DRAWBASKET */
    {
        if(listqtd == 0)
            return BASKETEMPTY; /* empty basket */
        sort = randmm(0, listqtd);
        ln = lcab;
        lant = NULL;
        for(j = 0; j < sort; j++)
        {
            lant = ln;
            ln = ln->prox;
        }
        sort = ln->num;         /* number drawn */
        listqtd--;

        if(listqtd <= 0)        /* if no more, dont need to delete head */
            return sort;

        if(lant != NULL)        /* not the head */
            lant->prox = ln->prox;
        else                    /* it is the head */
        {
            lcab = lcab->prox;
            lista = lcab;
        }
        free(ln);

        return sort;
    }
    else if(n == LISTBASKET)   /* LISTBASKET  */
    {
        /* for(a=randnorep(LISTBASKET); a!=BASKETLISTED; a=randnorep(LISTBASKET)) ( */
        if(listqtd == 0)
            return BASKETLISTED;    /* listed because empty basket */
        if(lista == NULL)
        {
            lista = lcab;
            return BASKETLISTED;    /* basket listed */
        }
        while(lista != NULL)
        {
            j = lista->num;
            lista = lista->prox;
            return j;
        }
    }
    return BASKETERROR;
}

/* muda o intervalo (libeco.h) */
double changemm(double min1, double max1, double min2, double max2, double x1)
{
    double x2, div;

    if(min1 == max1)
        return 0;
    div = (min2 - max2) / (min1 - max1);
    x2 = max2 + x1 * div - max1 * div;
    return x2;
}
