/* libinterpola testa funcoes dos alunos
 *  Compilar com:
 * $ gcc -fPIC libvinteumv2-1.c -shared -o libvinteumv2-1.so -Wall -O0
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define TOTAL 25

typedef struct slist
{
  int n;
  struct slist *prox;
}tlist;

/* 0 nome: Ruben */
int vinteum0(tlist *lmao, tlist *lmesa) { return 1; }

/* 1 nome: Alberto Cabus */
int vinteum1(tlist *lmao, tlist *lmesa)
{
    int media=0,a=0,pontuacao=0; //media para calculo de probabilidade, "AS" transformado(1) ou nao(0) em 10 , pontuacao;
    tlist *mao=lmao;
    tlist *mesa=lmesa;
    while(mao!=NULL){ //inicio varredura lmao
        if(mao->n==1){
            media=media-1; //calculando media para usar estrategia HI-LO
            if((pontuacao+11)<=21&&a==0){
                pontuacao+=11;
                a=1;
            }
            else
                pontuacao+=1;
        }
        else
        {
            if((pontuacao+mao->n)>21){
                a=0;            //fazer o "As" passar do valor 11 para 1
                pontuacao-=10;  //fazer o "As" passar do valor 11 para 1
            }
            pontuacao+=mao->n;
        }
        if(mao->n>=2&&mao->n<=6)//calculando media para usar estrategia HI-LO
            media++;
        if(mao->n==10)//calculando media para usar estrategia HI-LO
            media--;
        mao=mao->prox;
    } //fim varredura lmao
    if(pontuacao<=10) //se pontuacao for menor ou igual a 10, pedir mais uma carta, sempre;
        return 1;
    if((pontuacao>10)&&(mesa==NULL)) //acima de 10 na primeira rodada, probabilidade de estourar muito alta.
        return 0; //encerrar rodada

    if(mesa!=NULL) { //inicio varredura da mesa
        while(mesa!=NULL)
        {
            if(mesa->n>=2&&mesa->n<=6)//calculando media para usar estrategia HI-LO
                media++;
            if(mesa->n==1||mesa->n==10)//calculando media para usar estrategia HI-LO
                media--;
            mesa=mesa->prox;
        }
        if(media<=-3) //pontuacao ja chegara aqui maior que 11
            return 1;
        else
            return 0;
    }//fim varredura da mesa
    return 0;
}

/* 2 nome: Anaysa */
int vinteum2(tlist *lmao, tlist *lmesa) 
{
	 int ace=0,asoma=0,carta,carta2,csoma;   //metodo de contar cartas ace
     if(lmao->prox==NULL) //primeira mao
          return 1;
     if(lmesa!=NULL){    //proximas maos
         carta=lmao->prox->n;
         if(carta<6)
             ace++;
         if(carta>8){
             ace-=1;
             asoma=ace;}
         return 1;
         carta2=lmao->prox->prox->n;
         csoma=carta+carta2;
         if(carta2<6)
             ace+=1;
         if(carta2>8)
             ace-=1;
         asoma+=ace;
         if(csoma<17&&asoma>0)
             return 1;
         if(asoma<=0)
             return 0;}
     return 0;
 }

/* 3 nome: Andrey */
int vinteum3(tlist *lmao, tlist *lmesa) { return 0; }

/* 4 nome: Angela */
int vinteum4(tlist *lmao, tlist *lmesa) { return 1; }
{
	tlist *pma=lmao;            //Ponteiro que aponta lista mao
    tlist *pme=lmesa;           //Ponteiro que aponta lista mesa
    int somap=0;                //Soma probabilidade das cartas
    int somac=0;                //Soma as cartas da mao
    int somam=0;                //Soma as cartas da mesa
    int cartamao;
    int cartamesa;
    
	while(pma!=NULL)
    {
        cartamao=pma->n;
        somac=somac+cartamao;   //Soma as cartas da mao com as anteriores
        pma=pma->prox;
    }
	if(somac<=11)
    	return 1;
    if(somac<=10 && cartamao==1)
        somac=somac+10;
     
    while(pme!=NULL)
	{
        cartamesa=pme->n;
        somam=somam+cartamesa;
        if(cartamesa>=2 && cartamesa<=6)
			 somap=somap+1;
        if(cartamesa>=9 && cartamesa<=10)
             somap=somap-1;

        pme=pme->prox;
     }

        if(somap>0 && somac<=20)
			return 0;
        if(somap<0 && somac<=15)
            return 1;

	return 0;
}
 
/* 5 nome: Augusto Fidelis */
int vinteum5(tlist *lmao, tlist *lmesa) 
{ 
	int prob[10]={4,4,4,4,4,4,4,4,4,16};
	int sum=0;
	int somamais=0,somamenos=0,aux=0;
	int i,j,k;
	//ponteiros para rodar nas listas
   	tlist *m= lmao->prox;//m avaça até NULL
   	tlist *at=lmao;//at até a última célula com valor válido
   	// no que tá rodando a inserção de valor para at e m é feita aqui (at=lmao; e m=lmao->prox;) Faz diferença?
   	while (m!=NULL)
   	{
  		sum+=at->n;//sum recebe o valor do somatório das cartas da mão até a última carta sorteada 
    	at = at->prox;
      	m = m->prox;
      	if(m == NULL)
      	{
   			sum+=at->n;//fix para o bug do término do while
      	}
   	}
   	tlist *table=lmesa;
   	if(lmesa==NULL)
   	{
   		if(sum > 14)//código normal para um baralho sem sorteios com probabilidade de 28/52 para valores inferiores ou iguais a 7
    	{
        	return 0;
    	}
    	else
    	{
    		return 1;
    	} 
	}
	if(table!=NULL)
	{
		while (table!=NULL)
   		{
    		switch (table->n)
    		{
    			case 1:
    				prob[0]--;
    				break;
    			case 2:
    				prob[1]--;
    				break;
    			case 3:
    				prob[2]--;
    				break;
    			case 4:
    				prob[3]--;
    				break;
				case 5:
					prob[4]--;
    				break;
    			case 6:
    				prob[5]--;
    				break;
    			case 7:
    				prob[6]--;
    				break;
    			case 8:
    				prob[7]--;
    				break;
    			case 9:
    				prob[8]--;
    				break;
    			case 10:
    				prob[9]--;;
    				break;
				default:
					break;
    		}
			table = table->prox;
      	}
      	for(i=0;i<10;i++)
      	{
      		for(j=0;j<=i;j++)
      		{
      			somamenos+=prob[j];
      		}
      		for(k=9;k>i;k--)
      		{
      			somamais+=prob[k];
      		}
      		if(somamais<=somamenos)
      		{
				aux=i;
				break;
    		}
			else
			{
				somamais=0;
				somamenos=0;
			}
		}
		if(sum > 21-aux)//código normal para um baralho com sorteios com probabilidade aleatória
    	{
        	return 0;
    	}
    	else
    	{
    		return 1;
		}
	}
	return 0;
}

/* 6 nome: Carlos Fernando */
int vinteum6(tlist *lmao, tlist *lmesa) { return 0; }

/* 7 nome: Eduardo Emery */
int vinteum7(tlist *lmao, tlist *lmesa)
{
    int i = 0, valorc = 21, espacoa = 0, espacou = 0, cartas[10] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 16}; 
    tlist *aux = lmesa;
    while(aux != NULL)
    {
                    cartas[aux->n - 1] -= 1;
                    aux = aux->prox;
    }
    aux = lmao;
    while(aux != NULL)
    {
                    cartas[aux->n - 1] -= 1;
                    valorc -= aux->n;
                    aux = aux->prox;
    }  
    for(i = 0; i < 10; i++)
    {
        if (i+1 <= valorc)
           espacoa += cartas[i];
        espacou += cartas[i];
    }
    if((espacoa + 0.0)/(espacou + 0.0) > 0.5)
        return 1;
    else
        return 0;
}

/* 8 nome: Elivan */
int vinteum8(tlist *lmao, tlist *lmesa) 
{
         int ace=0;
         int soma=0,x;

         while(lmao!=NULL)
         {
             soma=soma + lmao->n;
             lmao=lmao->prox;
         }

         while(lmesa!=NULL)
         {
         x=lmesa->n;
          if ((x==1||x==2||x==3||x==4||x==5))
               ace = ace+1;
          if ((x==9||x==10))
               ace = ace-1;
         lmesa=lmesa->prox;
         }

         if (soma<=11)
             return 1;
         if (soma>=17);
             return 0;
         if ((soma == 12||soma == 13||soma == 14||soma == 15||soma==16))
         {
             if (ace<0)
                 return 1;
             else
                 return 0;
       }

 }

/* 9 nome: Emanuel */
int vinteum9(tlist *lmao, tlist *lmesa) { return 0; }

/* 10 nome: Italo */
int vinteum10(tlist *lmao, tlist *lmesa)
{
    int vmao[20] = {0}, vmesa[40] = {0}, cont1 = 0, cont2 = 0, conta = 1, soma = 0;
    int contp = 0, contg = 0;
    while (lmao != NULL)
    {
        vmao[cont1] = lmao->n;
        lmao = lmao->prox;
        cont1++;
    }
    while (lmesa != NULL)
    {
        vmesa[cont2] = lmesa->n;
        lmesa = lmesa->prox;
        cont2++;
    }
    cont1 = 0;
    while (vmao[cont1] != 0)
    {
        if(vmao[cont1] == 1)
        {
            if(conta == 1)
            {
                conta++;
                if(soma<=10)
                    soma += 11;
                else
                    soma += 1;
            }
            else
                soma += vmao[cont1];
        }
                cont1++;
    }
    if ( soma >= 15)
        return 0;
    cont2 = 0;
    while (vmesa[cont2] != 0)
    {
        if (vmesa[cont2] <= 6)
            contp++;
        else
            contg++;
                cont2++;
    }
    if (contp>contg)
        return 0;
    else
        return 1;
}


/* 11 nome: Jefferson */
int vinteum11( tlist *lmao, tlist *lmesa)
{
	int as, csoma, i, esp;
	csoma=0;
	esp=0;
	as = 0; /* Código verddade/falso inicializa a vari´vel "as" com zero(falso) indicando que o Ás não está na mão */ 
	
	while(lmao!=NULL) /* Verficamos as cartas e somamos os pontos da mão com o Laço While */
    {
        if(lmao->n==1) /* Se possuir um Ás na mão, então a variável As tem valor 1(verdade) atribuída a si mesma */
			as = 1;
        csoma += lmao->n; /* contador de pontos nas mãos */
        lmao = lmao->prox; /* passa paro elemento seguinte da lista */
    }
   	while(lmesa!=NULL)
	{
		if(lmesa->n > 6)
			esp+=1;
		else if(lmesa->n <= 6)
			esp-=1;
		lmesa=lmesa->prox;		
	}
	if(csoma<11)
		return 1;
	else if((csoma==11)&&(as)) /*Se possuir 11 pontos e o Ás incluso não preciso mais de pontos*/
		return 0;
	else if((csoma==11)&&(!as))	/*Possuindo 11 pontos e não tendo o Ás, EU QUERO CARTA!!!*/
		return 1;
	for(i=0; i<4; i++)							/*Aqui no laço FOR verifico todas as condições possíveis para todos os pontos em mão*/
	{
		if((csoma==(12+i))&&(esp>=0))	/*iclusive se tiver 11 pontos sem o Ás*/
			return 1;
		else
			return 0;
	}
	return 0;
}
/* 12 nome: Joao Francisco */
int vinteum12(tlist *lmao, tlist *lmesa)
{
  int vmao[20] = {0}, prob, vmesa[40] = {0}, cont1 = 0, cont2 = 0, soma = 0, conta = 1;
  while (lmao != NULL)
  {
      vmao[cont1] = lmao->n;
      lmao = lmao->prox;
      cont1++;
  }
  while (lmesa != NULL)
  {
      vmesa[cont2] = lmesa->n;
      lmesa = lmesa->prox;
      cont2++;
  }
  cont1 = 0;
  while (vmao[cont1] != 0)
  {
     if (vmao[cont1] == 1)
     {
          if (conta == 1)
          {
              conta++;
              if (soma <= 10)
                 soma += 11;
              else
                  soma += 1;
          }
          else
             soma += vmao[cont1];
      }
      cont1++;
   }
  if(soma <= 11)
     return 0;
  prob = 0;
  cont2 = 0;
  while(vmesa[cont2] != 0)
  {
     if(vmesa[cont2] <= 5)
         prob += 1;
     if(vmesa[cont2] >= 8)
         prob = prob - 1;
  }
  if (prob > 2)
  {
      if (soma < 15)
          return 1;
      else
          return 0;
  }
  if (prob < -2)
  {
      if (soma < 17)
          return 1;
      else
          return 0;
  }
  else
  {
      if (soma < 14)
          return 1;
      else
          return 0;
  }
}

/* 13 nome: Joao Paulo Silvino */
int vinteum13(tlist *lmao, tlist *lmesa) 
{
    int maoat; //Carta atual na mao
    int smao=0; //Soma das cartas da minha mao
    int auxas=0; //Flag - Indica se há as na mão
    int mesaat; //Carta atual na mesa
    int probab[10]; //Vetor utilizado para definir probabilidade
                    //probab[0]=> Numero de cartas As na mesa
                    //probab[1]=> Numero de cartas 2 na mesa
                    //probab[2]=> Numero de cartas 3 na mesa
                    //probab[3]=> Numero de cartas 4 na mesa
                    //probab[4]=> Numero de cartas 5 na mesa
                    //probab[5]=> Numero de cartas 6 na mesa
                    //probab[6]=> Numero de cartas 7 na mesa
                    //probab[7]=> Numero de cartas 8 na mesa
                    //probab[8]=> Numero de cartas 9 na mesa
                    //probab[9]=> Numero de cartas que valem 10 pontos na mesa
    int caresp; // Carta esperada (completa 21 pontos em relação as cartas da mao)
    int nmesa=0; //Número de cartas na mesa
    int aux; // Contador
    int nme=0; //Número de cartas no deck com valor menor ou igual ao da carta esperada
    int nma=0; //Número de cartas no deck com valor maior que o da carta esperada
    tlist *pmao=lmao; // Ponteiro que percorre lista mao
    tlist *pmesa=lmesa; //Ponteiro que percorre lista mesa

    /*Percorre as cartas da mao e dá a soma*/
    while(pmao!=NULL)
    {
        maoat=pmao->n;
        smao=smao+maoat;
        if(maoat==1)//Flag - Indica se há as na mao
            auxas=1;
        pmao=pmao->prox;
    }
    if(auxas==1 && smao<=11) //Atribui valor 11 ao as, quando necessário
        smao=smao+10;

    /*Retorna 1 quando a soma das cartas na mao <= 11*/
    if(smao<=11)
        return 1;

    /*Define quantidade de cartas de cada valor*/
    for(aux=0; aux<9; aux++)
        probab[aux]=4;
    probab[9]=16;

    /*Percorre as cartas da mesa*/
    while(pmesa!=NULL)
    {
		mesaat=pmesa->n;
	    probab[mesaat-1]--;
        nmesa++;
        pmesa=pmesa->prox;
    }

    /*Definição da carta esperada*/
    caresp=21-smao;

    /*Número de cartas no deck*/
    for(aux=0; aux<=9; aux++)
		if(aux+1<=caresp)
			nme=nme+probab[aux];
		else
			nma=nma+probab[aux];

	/*Escolha da carta de acordo com a probabilidade de ganhar*/
	if(nme>=nma)
		return 1; //Mais chances de receber carta de valor menor ou igual a     carta esperada

	return 0; //Menos chances de receber carta de valor menor ou igual a car    ta esperada
}

/* 14 nome: Joao Pedro */
int vinteum14(tlist *lmao, tlist *lmesa)
{
    int vmao[20] = {0}, vmesa[40] = {0}, cont1 = 0, cont2 = 0;
    int probb = 0, soma = 0, conta = 0, i;

    while (lmao != NULL)
    {
        vmao[cont1] = lmao->n;
        lmao = lmao->prox;
        cont1++;
    }
    while (lmesa != NULL)
    {
        vmesa[cont2] = lmesa->n;
        lmesa = lmesa->prox;
        cont2++;
    }

    i = 0;
    while(vmesa[i] != 0)
    {
        if(vmesa[i] == 1)
        {
            conta++;
            if(conta == 1)
            {
                if(soma<=10)
                    soma += 11;
                else
                    soma += vmesa[i];
            }
        }
        else
            soma += vmesa[i];
        i++;
    }

   for(i = 0; i <= cont2; i++)
   {
       if(vmesa[i] >= 9)
           probb++;
       if(vmesa[i] <= 5)
           probb--;
   }

   if(soma >= 21)
       return 0;

   if(probb >= 3)
   {
       if(soma <= 14)
			return 1;
		else
			return 0;
	}
	if( prob <= -3)
	{
		if(soma <= 11)
			return 1;
		else
			return 0;
	}
	else
	{
		if(soma <= 15)
			return 1;
		else
			return 0;
	}
}

/* 15 nome: Jose Marcos */
int vinteum15(tlist *lmao, tlist *lmesa) { return 1; }

/* 16 nome: Kae */
int vinteum16(tlist *lmao, tlist *lmesa) { return 0; }

/* 17 nome: Mauricio */
int vinteum17(tlist *lmao, tlist *lmesa) { return 0; }

/* 18 nome: Nadja */
int vinteum18(tlist *lmao, tlist *lmesa) { return 1; }
 {
    int jj0, jj1; 
    int i;
    int mao[2]; /* da mao */
    int rmao[2]; /* da rodada 5 maos */
    tamanho do maior nome
    int tmao[i],tumpt[i];
              if(mao[0]!=mao[1]) // else empate nao da ponto
            {
          if(mao[0]<=21 && mao[1]<=21) // sao validos!
            {
               if(mao[0]>mao[1]) //mao 0 ganhou
               {
                      tmao[jj0] += mao[0]; /* total geral */
                      rmao[0] += mao[0]; /* total da rodada */
                      tumpt[jj0]++;
     }
           else //mao 1 ganhou
      {
                     tmao[jj1] += mao[1];
                     rmao[1] += mao[1];
                     tumpt[jj1]++;
      }
                      }
                else // alguem estourou
                      {
            if(mao[0]<=21) 
            {
                     tmao[jj0] += mao[0];
                     rmao[1] += mao[1];
                     tumpt[jj1]++;
            }
                     }
            else 
                      {
                     if(mao[0]<=21) 
                     {
                           tmao[jj0] += mao[0];
                           rmao[0] += mao[0];
                           tumpt[jj0]++;
            } 
                 if(mao[1]<=21) 
 				 {
                       tmao[jj1] += mao[1];
                       rmao[1] += mao[1];
                       tumpt[jj1]++;
 
 return 0;
 }//
 }//


/* 19 nome: Paulo Ricardo */
int vinteum19(tlist *lmao, tlist *lmesa) { return 0; }

/* 20 nome: Petrus */
int vinteum20(tlist *lmao, tlist *lmesa) {

	const int DEBUG = 0;
	
	int retorno;
	float ganha = 0, perde = 0, pGanha, pPerde, pAs;
	int somaParcial = 0, somaTotal = 0;
	int as = 0;
	tlist *lista;
	lista = lmao;
	int baralho[10] = {4, 4, 4, 4, 4, 4, 4, 4, 4, 16};	//possibilidades de cartas no baralho
	int i=0;
	
	while(lista != NULL){
		if(lista->n == 1)
			as++;
		else
			somaParcial += lista->n;
		lista = lista->prox;
	}
	
	if(as){
	
		if( (somaParcial + as + 10) <= 21 )	//nao estourou, entao o as vale 11
			somaTotal = (somaParcial + as + 10);
		else	//estourou, logo o as vale apenas 1
			somaTotal = (somaParcial + as);
	}else
		somaTotal = somaParcial;
	
	if(DEBUG)
		printf("(minha soma = %d)\n", somaTotal);	//DEBUG
	
	if(lmao == NULL){
		retorno = 1;//quero carta na primeira rodada
	}else{
		if(somaTotal <= 11)
			retorno = 1;//nao tem como perder pois a proxima carta nao pode valer mais do que 10
		else if (somaTotal > 11 ){
			lista = lmao;
			while(lista != NULL){
				baralho[(lista->n)-1]--;	//decrementa do baralho as cartas que estao na mao
				lista = lista->prox;
			}
			lista = lmesa;
			while(lista != NULL){
				baralho[(lista->n)-1]--;	//decrementa do baralho as cartas que estao na mesa
				lista = lista->prox;
			}
			
			if(DEBUG){
				printf("baralho = {");
				printf("%d, ",baralho[0]);
				printf("%d, ",baralho[1]);
				printf("%d, ",baralho[2]);
				printf("%d, ",baralho[3]);
				printf("%d, ",baralho[4]);
				printf("%d, ",baralho[5]);
				printf("%d, ",baralho[6]);
				printf("%d, ",baralho[7]);
				printf("%d, ",baralho[8]);
				printf("%d}\n",baralho[9]);
			}
			
			
			for(i=1; i<=10 ; i++){
				if( (somaTotal + i) <= 21)
					ganha += baralho[i-1];	//adiciona ao indice ganha o numero de cartas que dao a possibilidade de ganhar
				else if( (somaTotal + i) > 21 )
					perde += baralho[i-1];	//adiciona ao indice perde o numero de cartas que dao a possibilidade de perder
			}

			pGanha = ganha/52;
			pPerde = perde/52;
			pAs = (float)baralho[0]/52;
			
			if(DEBUG){
				printf("ganha = %.0f, ", ganha);
				printf("perde = %.0f\n", perde);			
				printf("pGanha = %.4f, ", pGanha);
				printf("pPerde = %.4f\n", pPerde);
				printf("pAs = %.2f\n", pAs);
			}
			
			
			if(as){
				if(pGanha > pPerde){
					retorno = 1;	//quero mais cartas se a probabilidade de ganhar é maior do que de perder
				}else if(pGanha <= pPerde){
					retorno = 0;	//se der empate na probabilidade ou for mais facil perder não quero mais cartas
				}
			}else{
				if(pGanha > (2 * pPerde)){
					retorno = 1;	//quero mais cartas se a probabilidade de ganhar é maior do que de perder
				}else if(pGanha <= ( 2 * pPerde) ){
					retorno = 0;	//se der empate na probabilidade ou for mais facil perder não quero mais cartas

				}
			}
		}
	}
	
	return retorno;
}

/* 21 nome: Rafael */
int vinteum21(tlist *lmao, tlist *lmesa) { 


     int somacartasmesa = 0, ncartasmesa = 0, ncartasmao = 0, retorno, somamao = 0;
     tlist *pmesa, *pmao;
     float esperanca;

     pmesa = lmesa;
     pmao = lmao;
     esperanca = 0;

     if(pmao == NULL){
         retorno = 1;
     }else{
             ncartasmesa= 1;
             while (pmesa != NULL){
                     somacartasmesa += pmesa->n;
                     pmesa = pmesa->prox;
                     ncartasmesa ++;
                     }
             while (pmao != NULL){
                     somamao += pmao->n;
                     pmao = pmao->prox;
                     ncartasmao ++;
                     }
             esperanca = (340-somacartasmesa)/(51-ncartasmesa); //(52 - ncartasmesa - 1) usei todas as 52 cartas com mesma     probabilidade...

             if(abs(esperanca)<(19-somamao))
                 retorno = 1;
             else
                 retorno = 0;



         }

     return retorno;
 }

/* 22 nome: Rodrigo */
int vinteum22(tlist *lmao, tlist *lmesa) {

	tlist *mao = lmao;
	tlist *mesa = lmesa;
    int cartas[25] = {0};
    int estrategiamao[52] = {0};
    int estrategiamesa[52] = {0};
    int somatorio = 0;
    int i = 0, cont = 0;
    int probabilidade = 0;
    int probabilidadeBaralho = 0;

    while(mao != NULL){     //encher vetor cartas
        cartas[i] = mao->n;
        i++;
        mao = mao->prox;
    }

    while(mesa != NULL){
        if((mesa->n == 2)||(mesa->n == 4)||(mesa->n == 6))     //atribuir valores p/ a estrategiamesa
            estrategiamesa[i] = 2;
        if(mesa->n == 5)
            estrategiamesa[i] = 3;
        if(mesa->n == 7)
            estrategiamesa[i] = 1;
        if(mesa->n == 8)
            estrategiamesa[i] = 0;
        if(mesa->n == 9)
            estrategiamesa[i] = -1;
        if((mesa->n == 10)||(mesa->n == 1))
            estrategiamesa[i] = -2;
        mesa = mesa->prox;
        probabilidadeBaralho =+ estrategiamesa[i];
        i++;
    }

    i=0;

    while(mao != (struct slist *) 0){
        if((mao->n == 2)||(mao->n == 4)||(mao->n == 6))     //atribuir valores p/ a estrategiamao
            estrategiamao[i] = 2;
        if(mao->n == 5)
            estrategiamao[i] = 3;
        if(mao->n == 7)
            estrategiamao[i] = 1;
        if(mao->n == 8)
            estrategiamao[i] = 0;
        if(mao->n == 9)
            estrategiamao[i] = -1;
        if((mao->n == 10)||(mao->n == 1))
            estrategiamao[i] = -2;
        mao = mao->prox;
        probabilidade =+ estrategiamao[i];
        i++;
    }

    for(cont=0;cont<25;cont++)
        somatorio =+ cartas[cont];      //total de pontos por chamada


    /* comeco de calculo das probabilidades */
    if(somatorio<11)
        return 1;

    if(probabilidadeBaralho < 0)
        probabilidade =+ 1;
    if(probabilidadeBaralho > 0)
        probabilidade =- 1;

    if((somatorio >= 11) && (somatorio <= 19)){       //pedir ou recusar cartas
        if(probabilidade < 0)
            return 0;
        else
            return 1;
    }
    else
        return 0;


}

/* 23 nome: Victor Hugo */
/* 23 nome: Victor Hugo */
int vinteum23(tlist *lmao, tlist *lmesa)
{
    //              VICTOR HUGO DE BARROS CABRAL
    //              HI/LO

    int pont=0, hilomesa=0;
    //contagem de pontos na mao
    tlist *p1=lmao;
    while(p1!=NULL)
    {
                        pont+=p1->n;
            p1=p1->prox;
    }

    //contagem hi-lo da mesa
    tlist *p2=lmesa;
    while(p2!=NULL)
    {
                   if((p2->n)==(2||3||4||5||6))
                         hilomesa++;
                   if((p2->n)==(10||1||11))
                         hilomesa--;
                                   p2=p2->prox;
    }


    if(pont<=(11 + hilomesa))
                 return 1;
    else
                 return 0;
}


/* 24 nome: Victor Rolim */
int vinteum24(tlist *lmao, tlist *lmesa) 
{
int soma=0;
tlist *temp = lmao;

while(temp!=NULL)
{
	soma += temp->n;
	temp = temp->prox;
}
	if (soma<12)
     		{
    		return 1;
     		}
	else
     		{
     		return 0;
  	   	}
}

/* -----------------------------------------------------*/
/* Funcoes de apoio. Não alterar. */

char *nomes[TOTAL]={"Ruben", "Alberto", "Anaysa", "Andrey", "Angela", "Augusto", "Carlos", "Eduardo", "Elivan", "Emanuel", "Italo", "Jefferson", "Francisco", "Joao", "Pedro", "Jose", "Kae", "Mauricio", "Nadja", "Paulo", "Petrus", "Rafael", "Rodrigo", "Hugo", "Victor"};

/* numero de funcoes: retorna o TOTAL definido acima */
int nfunc(void)
{
  return TOTAL;
}

/* retorna o nome de um dado jogador */
char *playernames(int x)
{
  return nomes[x];
}

/* tamanho da string do maior nome */
int biggername(void)
{
  int i, l;
  int s=strlen(nomes[0]);
  for(i=1; i<TOTAL; i++)
  {
	l=strlen(nomes[i]);
	if(s<l)
	  s=l;
  }
  return s;
}

